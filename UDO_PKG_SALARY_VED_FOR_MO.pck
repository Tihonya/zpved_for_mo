create or replace package UDO_PKG_SALARY_VED_FOR_MOV2 is

  -- Author  : SAVCHENKO
  -- Created : 24.10.2017 13:46:26
  -- Purpose : ������� ��� ���������� ���� "������������-���Ҳ��� ²������� � 1" ��� ̳��������

  -- Public type declarations
  -- type <TypeName> is <Datatype>;

  -- Public constant declarations
  -- <ConstantName> constant <Datatype> := <Value>;

  -- Public variable declarations
  --<VariableName> <Datatype>;

  -- Public type declarations

  function CLNPERSEXP_GET_LEN(nCOMPANY     in number,
                              nEXPERIENCES in number,
                              nCLNPERSONS  in number,
                              dCALCDATE    in date) return varchar2;

  ---------------------------------------------------------

  procedure GetPersonalData_A3( /*NVed*/IIDENT in number,
                               CUR    in out sys_refcursor);

  Procedure Get_category_cursor_A2(aCursor out pkg_cursors.CurType);

  procedure GetHeadsOutsGr_A6( /*NVed*/IIDENT in number,
                              CUR    in out sys_refcursor);

  procedure GetHeadsAddsGr_A6( /*NVed*/IIDENT in number,
                              CUR    in out sys_refcursor);

  procedure Get_GRAddsVal_A4( /*NVed*/IIDENT  in number,
                             AGNRN   in number,
                             sSgCode in varchar2,
                             CUR     in out sys_refcursor);

  procedure Get_GROutsVal_A4( /*NVed*/IIDENT  in number,
                             AGNRN   in number,
                             sSgCode in varchar2,
                             CUR     in out sys_refcursor);

  Function Udo_F_Get_Fot_Ondate(Nprn      In Number,
                                Sgrsalary In Varchar2,
                                Ddate     In Date) Return Number;
                                
 procedure Get_Pay_Paid_List(IIDENT in number, CUR in out sys_refcursor);                               

  FUNCTION FU_AGNRANK_ONDATE(NAGNLIST IN NUMBER,
                             DDATE    IN DATE DEFAULT SYSDATE,
                             SPAD     IN VARCHAR2 DEFAULT '�')
    RETURN VARCHAR2;

  FUNCTION FU_GET_PREMPFLS_EXP_ONDATE(NAGNLIST IN NUMBER, dondate in date)
    RETURN VARCHAR2;

  function GetSummAddsByAgnRn( /*NVed*/ IIDENT in number, AGNRN in number)
    return number;

  function GetSumm_Outs_By_AgnRn( /*NVed*/IIDENT in number,
                                 AGNRN  in number) return number;

  function Get_Val_By_GR(ofc_numbb in number, sSgCode in varchar2)
    return number;

  function GetCOUNTHead_GR_Adds(IIDENT /*NVed*/ in number) return number;

  function GetCOUNTHead_GR_Outs(IIDENT /*NVed*/ in number) return number;

  function GetSummOutSBy_Category(IIDENT   /*NVed*/    in number,
                                  Ofc_numbb in number) return number;

  function GetSummAddsBy_Category(IIDENT   /*NVed*/    in number,
                                  Ofc_numbb in number) return number;

  function GetSumm_Bank_By_Category(IIDENT   /*NVed*/    in number,
                                    Ofc_numbb in number) return number;

  function GetSummK_PDFO_By_Category(IIDENT   /*NVed*/    in number,
                                     Ofc_numbb in number) return number;

  function GetSumm_ONHand_Category(IIDENT   /*NVed*/    in number,
                                   Ofc_numbb in number) return number;

end UDO_PKG_SALARY_VED_FOR_MOV2;
/
create or replace package body UDO_PKG_SALARY_VED_FOR_MOV2 is
  --����� �� �������������� ������ ����� ��������� ������ ������
  --� ���� ������ ��� �������� ����� ����� ������,
  --� ����� ������� ������� - ��� ����� ����� �� ����������
  /* nEXPERIENCES number(17);
    --dDATE        date;
    sEXPERIENCES    varchar2(40) := '�������'; --'�������'; --'���� �������������.';
    sCLNPERSEXP     varchar2(20);
    nCOMPANY        number(17);
    nheaderRowAdd   number(2) := 17;
    nheaderRowSub   number(2) := 9;
    nFirst_page     number(2) := 9; --28; --����� �� ������ ��������
    nNext_page      number(2) := 16; --42; --����� �� ���������
    sFOTEXPERIENCES varchar2(40) := '������� ���� (��)';
    max_Row         number(2);
    max_page        number(2);
  */
  
  sPAY_GROUP_ID     varchar2(10) :='49';
  
  function CLNPERSEXP_GET_LEN(nCOMPANY     in number, -- �����������
                              nEXPERIENCES in number, -- ���. ����� ���� �����
                              nCLNPERSONS  in number, -- ���. ����� ����������
                              dCALCDATE    in date -- ���� �� ������� ������������� ����
                              ) return varchar2 as
    nDAYS     PKG_STD.tNUMBER;
    nMONTHS   PKG_STD.tNUMBER;
    nYEARS    PKG_STD.tNUMBER;
    dFirstDay date;
  begin
    --- �� ������������ � ������, ������ ����� ��������
    dFirstDay := int2date(1,
                          extract(month from dCALCDATE),
                          extract(year from dCALCDATE));
    /* ����������� ���������� ���, ����, ������� ���������� ����� � ���������� */
    PKG_CLNPERSEXP.GETEXP(nEXPERIENCES,
                          nCLNPERSONS,
                          --dCALCDATE,
                          dFirstDay,
                          nDAYS,
                          nMONTHS,
                          nYEARS);
  
    /* ������������ ������ ������� "Y � M � D �" */
    return to_char(nYEARS) || ' � ' || to_char(nMONTHS) || ' � ' || to_char(nDAYS) || ' �';
  
  end;
  --------------------------------------------------------------------

  Function Udo_F_Get_Fot_Ondate(Nprn      In Number,
                                Sgrsalary In Varchar2,
                                Ddate     In Date) Return Number Is
    Ngrsalary Number(17);
    Ncoef     Number(17, 2);
  
  Begin
    Begin
      Select G.Rn
        Into Ngrsalary
        From Grsalary g
       Where trim(G.Code) = Sgrsalary;
    
    Exception
      When Others Then
        Ngrsalary := 0;
    End;
  
    Begin
      Select Gs.Coeffic
        Into Ncoef
        From Clnpspfmgs Gs
       Where Gs.Prn = Nprn
         And Gs.Grsalary = Ngrsalary --411122858--
         And ((Gs.Do_Act_From <= Ddate) And
             ((Gs.Do_Act_To >= Ddate) Or (Gs.Do_Act_To Is Null)));
    
    Exception
      When Others Then
        Ncoef := 0;
    End;
  
    Return Ncoef;
  
  End;
  --------------------------------------------------------------
  FUNCTION FU_GET_PREMPFLS_EXP_ONDATE(NAGNLIST IN NUMBER, dondate in date)
    RETURN VARCHAR2 IS
    SRESULT VARCHAR2(20);
    NDAYS   NUMBER(2);
    NMONTHS NUMBER(2);
    NYEARS  NUMBER(4);
    NPERSRN NUMBER;
  
  BEGIN
    BEGIN
      SELECT C.RN
        INTO NPERSRN
        FROM CLNPERSONS C
       WHERE C.PERS_AGENT = NAGNLIST
         AND (C.DISMISS_DATE IS NULL or c.dismiss_date >= dondate);
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RETURN NULL;
      WHEN TOO_MANY_ROWS THEN
        RETURN '���� ������� �����������';
    END;
  
    IF NPERSRN IS NOT NULL THEN
      PKG_CLNPERSEXP.GETEXP(NEXPERIENCES => 267643,
                            NCLNPERSONS  => NPERSRN,
                            DEND         => dondate,
                            NDAYS        => NDAYS,
                            NMONTHS      => NMONTHS,
                            NYEARS       => NYEARS);
    END IF;
    IF NYEARS + NMONTHS + NDAYS = 0 THEN
      SRESULT := '�� �������';
    ELSE
      SRESULT := NYEARS || ' � ' || NMONTHS || ' � ' || NDAYS || ' �';
    END IF;
    --sResult := nyears||' � '||nmonths||' � '||ndays||' �';
  
    RETURN(SRESULT);
  END;
  -----------------------------------------------------------------
  FUNCTION FU_AGNRANK_ONDATE(NAGNLIST IN NUMBER,
                             DDATE    IN DATE DEFAULT SYSDATE,
                             SPAD     IN VARCHAR2 DEFAULT '�')
    RETURN VARCHAR2 AS
    SRESULT VARCHAR2(250);
  BEGIN
  
    BEGIN
      SELECT DECODE(SPAD,
                    '�',
                    SL.NAME,
                    '�',
                    sl.NAME_GEN,
                    '�',
                    sl.NAME_DAT,
                    '�',
                    sl.NAME_ACC,
                    '�',
                    sl.NAME_ABL,
                    '�',
                    sl.NAME_FOR,
                    SL.NAME)
        INTO SRESULT
        FROM AGNRANKS A, SLRANK SL
       WHERE A.BEGIN_DATE IN (SELECT MAX(SP2.BEGIN_DATE)
                                FROM AGNRANKS SP2
                               WHERE SP2.BEGIN_DATE <= NVL(DDATE, SYSDATE)
                                 AND SP2.PRN = NAGNLIST)
         AND A.PRN = NAGNLIST
         AND A.RANK = SL.RN;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        SRESULT := '';
    END;
  
    RETURN SRESULT;
  
  END;
  --------------------------------------------------------------------

  procedure GetPersonalData_A3(IIDENT in number, CUR in out sys_refcursor) as
  begin
    --   open CUR for
    delete from UDO_T_VED_MO_FOR_FR2;
    commit;
    insert into UDO_T_VED_MO_FOR_FR2
      select --0,
       al.agnfamilyname || ' ' || substr(trim(al.agnfirstname), 0, 1) || '. ' ||
       substr(trim(al.agnlastname), 0, 1) || '.' fullAgnName,
       t.rn vRn,
       t.docdate,
       cp.jobbegin_date,
       --  u.code,
       --     trim(t.pref),
       --     trim(t.numb),
       --    sg.name  sMnemo,
       nvl(cdep.psdep_name, p.name) Shtatna_Posada,
       --   FU_AGNRANK_ONDATE(NAGNLIST => al.rn,
       --                   DDATE    => TRUNC(t.docdate, 'Month')) Rank,
       --  FU_GET_PREMPFLS_EXP_ONDATE(NAGNLIST => al.rn,
       --   dondate  => TRUNC(t.docdate, 'Month')) EXP_ONDATE,
       ofc.ordnumb Ofc_numb,
       ofc.name    Ofc_name,
       
       --         sc.Code Mnemo_PayPaid,
       --         sc.name Name_PayPaid,
       --         sc.numb numCode_PayPaid,
       --         sc.compch_type markType_PayPaid,
       ts.summ summa,
       sg.name gr_payPaid_name,
       sg.code gr_payPaid_code,
       --         agr.begin_date rang_begin_date,
       al.rn      agn_rn,
       al.agnname,
       --           t.docdate Ved_date,
       --          rang.name rang,
       
       --    UDO_F_GET_FOT_ONDATE(Nprn      => fm.rn,
       --                         Sgrsalary => '������� ���� (��)',
       --                         Ddate     => greatest(TRUNC(t.docdate, 'Month'),
       --                                                cp.jobbegin_date)) visotokMON_Add,
       ts.month,
       fm.rn    fm_rn
      
        from slpsheets t,
             slpsheetspfm tp,
             slpsheetspfmspecs ts,
             slcompcharges sc,
             slcompgr sg,
             slcompgrstruct sgs,
             clnpspfm fm,
             clnpsdep cdep, --added
             clnposts p,
             clnpersons cp,
             agnlist al,
             agnranks agr, --added
             SLRANK rang, --added
             V_INS_DEPARTMENT D,
             jurpersons u,
             doctypes dt,
             SLCALCULAT scal,
             SLANLSIGNS sanl,
             Officercls ofc,
             (select * from selectlist sls where sls.ident = IIDENT) slsl
      
       where t.rn = tp.prn
         and t.rn = slsl.document -- NVed -- 2000563654 --sl.document
         and tp.rn = ts.prn
         and t.jurpersons = u.rn
         and tp.CLNPSPFM = fm.rn
         and fm.persrn = cp.rn
         and fm.officercls = ofc.rn(+)
         and fm.psdeprn = cdep.rn(+) --added      
         and cp.pers_agent = al.rn
         and al.rn = agr.prn(+) --added
         and agr.rank = rang.rn
         and fm.postrn = p.rn(+)
         and ts.slcompcharges = sc.rn
         and substr(sg.code, 1, 2) = sPAY_GROUP_ID 
            --   or  sg.code = '5 �������������+���')
         and t.doctypes = dt.rn
            --and (sDEPCODE is null or instr(sDEPCODE, D.code) > 0)
         and sg.rn = sgs.prn(+)
         and fm.DEPTRN = D.RN(+)
         and sgs.slcompcharges = ts.slcompcharges(+)
         and t.slcalculat = scal.rn
         and ts.slanlsigns4 = sanl.rn(+)
       order by al.agnname;
    commit;
  
    open CUR for
      select GGG.* /*distinct GGG.FULLAGNNAME, GGG.JOBBEGIN_DATE, GGG.SHTATNA_POSADA, GGG.OFC_NUMB, 
                                                          GGG.Ofc_Name, GGG.AGN_RN, GGG.Agnname, */, --,'Month') ,,
             UDO_PKG_SALARY_VED_FOR_MOV2.UDO_F_GET_FOT_ONDATE(Nprn      => GGG.Fm_Rn,
                                                              Sgrsalary => '������� ���� (��)',
                                                              Ddate     => greatest(GGG.TargetDate /*TRUNC(GGG.DOCDATE,
                                                                                                                                                                                                                                                                                                                                                                                                                                          'Month')*/,
                                                                                    GGG.jobbegin_date)) visotokMON_Add,
             UDO_PKG_SALARY_VED_FOR_MOV2.FU_AGNRANK_ONDATE(NAGNLIST => GGG.agn_rn,
                                                           DDATE    => GGG.TargetDate /*TRUNC(GGG.docdate, 'Month')*/) Rank,
             UDO_PKG_SALARY_VED_FOR_MOV2.FU_GET_PREMPFLS_EXP_ONDATE(NAGNLIST => GGG.agn_rn,
                                                                    dondate  => GGG.TargetDate /*TRUNC(GGG.docdate,'Month')*/) EXP_ONDATE,
             (select LISTAGG(AAA.jname, '; ') within group(order by AAA.jname) jname
                from (select distinct nvl(j.code, j.name) jname
                        from selectlist slsl, slpsheets slp, jurpersons j
                       where slsl.ident = IIDENT
                         and slsl.document = slp.rn
                         and slp.jurpersons = j.rn) AAA) Jname
        from (select distinct utf.fullagnname,
                              -- utf.vrn,
                              -- utf.docdate,
                              utf.jobbegin_date,
                              utf.shtatna_posada,
                              utf.ofc_numb,
                              utf.ofc_name,
                              (select TRUNC(MAX(utff.docdate), 'Month')
                                 from UDO_T_VED_MO_FOR_FR2 utff) TargetDate,
                              --   utf.summa,
                              --   utf.gr_paypaid_name,
                              --   utf.gr_paypaid_code,
                              utf.agn_rn,
                              utf.agnname,
                              utf.fm_rn
              
              --    utf.month
                from UDO_T_VED_MO_FOR_FR2 utf) GGG
       order by GGG.fullagnname;
  
  end GetPersonalData_A3;

  ------------------------------------------------------------
   procedure Get_Pay_Paid_List(IIDENT in number, CUR in out sys_refcursor) as
  begin
    open CUR for
      select --0,
      --  al.agnfamilyname || ' ' || substr(trim(al.agnfirstname),0,1) || '. ' || substr(trim(al.agnlastname),0,1) || '.' fullAgnName,
      --  u.code,
      --     trim(t.pref),
      --     trim(t.numb),
      --    sg.name  sMnemo,
      --          cdep.psdep_name  Shtatna_Posada,
      --          FU_AGNRANK_ONDATE(NAGNLIST => al.rn,
      --                            DDATE    => t.docdate) Rank,
      --          FU_GET_PREMPFLS_EXP_ONDATE(NAGNLIST => al.rn, dondate => t.docdate )
      --          EXP_ONDATE,            
      distinct sc.Name Name_, sc.numb, sc.code, sc.compch_type  --,        
      --         sc.compch_type markType_PayPaid,
      --         ts.summ summa,
      --         sg.name gr_payPaid_name,
      --         sg.code gr_payPaid_code--,
      --         agr.begin_date rang_begin_date,
      --         al.rn agn_rn,
      --         al.agnname ,
      --           t.docdate Ved_date,
      --          rang.name rang,
      --         'StatVal 0%' visotokMON_Add 
      --  decode(substr(sg.code, 4, 2), '50', 99, sc.compch_type) compch_type,
      --     decode(substr(sg.code, 4, 2), '99', 99,
      --            sc.compch_type) compch_type,
      --     al.rn,             --al.agnname,
      --     tp.clnpspfm,
      --     t.docdate, --trunc(t.docdate, 'MM'),
      --     sum(ts.summ) nSum,
      --     tp.SUMMCHARDE,
      --     tp.SUMMRETAIN,
      --     nvl(tp.SUMMCHARDE, 0) + nvl(tp.SUMMADVANCE, 0) -
      --     nvl(tp.SUMMRETAIN, 0) + nvl(tp.SUMMOVERPAY, 0) nSUMAll,
      --     D.name,
      --     D.rn,
      --     t.rn,
      --     t.docdate,
      --     dt.doccode,
      --     scal.year VED_YEAR,
      --     scal.monthnumb VED_MONTH,
      --     t.docdate VED_DATE,
      --     TS.SLCOMPCHARGES NSLCOMPCH /*z*/
      --    into  nRESULT
        from -- (select sellist.document from selectlist sellist where sellist.ident=IDENT) sl,
             slpsheets         t,
             slpsheetspfm      tp,
             slpsheetspfmspecs ts,
             slcompcharges     sc,
             slcompgr          sg,
             slcompgrstruct    sgs,
             clnpspfm          fm,
             clnpsdep          cdep, --added
             clnposts          p,
             clnpersons        cp,
             agnlist           al,
             agnranks          agr, --added
             SLRANK            rang, --added
             V_INS_DEPARTMENT  D,
             jurpersons        u,
             doctypes          dt,
             SLCALCULAT        scal,
             SLANLSIGNS        sanl,
             (select sellist.document from selectlist sellist where sellist.ident=IIDENT) slsl
       where t.rn = tp.prn
         and t.rn =  slsl.document--NVed -- 2000563654 --sl.document
         and tp.rn = ts.prn
         and t.jurpersons = u.rn
         and tp.CLNPSPFM = fm.rn
         and fm.persrn = cp.rn
         and fm.psdeprn = cdep.rn --added
         and cp.pers_agent = al.rn
         and al.rn = agr.prn(+) --added
         and agr.rank = rang.rn
         and fm.postrn = p.rn(+)
         and ts.slcompcharges = sc.rn
            --      and (substr(sg.code, 1, 2) = '49' or sg.code='5 �������������+���' or sc.numb=71 )
         and t.doctypes = dt.rn
            --and (sDEPCODE is null or instr(sDEPCODE, D.code) > 0)
         and sg.rn = sgs.prn(+)
         and fm.DEPTRN = D.RN(+)
         and sgs.slcompcharges = ts.slcompcharges(+)
         and t.slcalculat = scal.rn
         and ts.slanlsigns4 = sanl.rn(+)
      --   and sc.compch_type = 10
      order by sc.compch_type;
  end Get_Pay_Paid_List;

  
  
  
  ---------------------------------------------------------------
  procedure GetPersonalData_A3______(IIDENT in number,
                                     CUR    in out sys_refcursor) as
  begin
  
    -- ����� ������ ������� ��������� ��� � ������������ ������������ � ������ ����������
    --   open CUR for
    delete from UDO_T_VED_MO_FOR_FR2;
    commit;
    insert into UDO_T_VED_MO_FOR_FR2
      select --0,
       al.agnfamilyname || ' ' || substr(trim(al.agnfirstname), 0, 1) || '. ' ||
       substr(trim(al.agnlastname), 0, 1) || '.' fullAgnName,
       t.rn vRn,
       t.docdate,
       cp.jobbegin_date,
       --  u.code,
       --     trim(t.pref),
       --     trim(t.numb),
       --    sg.name  sMnemo,
       nvl(cdep.psdep_name, p.name) Shtatna_Posada,
       --   FU_AGNRANK_ONDATE(NAGNLIST => al.rn,
       --                   DDATE    => TRUNC(t.docdate, 'Month')) Rank,
       --  FU_GET_PREMPFLS_EXP_ONDATE(NAGNLIST => al.rn,
       --   dondate  => TRUNC(t.docdate, 'Month')) EXP_ONDATE,
       ofc.ordnumb Ofc_numb,
       ofc.name    Ofc_name,
       
       --         sc.Code Mnemo_PayPaid,
       --         sc.name Name_PayPaid,
       --         sc.numb numCode_PayPaid,
       --         sc.compch_type markType_PayPaid,
       ts.summ summa,
       sg.name gr_payPaid_name,
       sg.code gr_payPaid_code,
       --         agr.begin_date rang_begin_date,
       al.rn      agn_rn,
       al.agnname,
       --           t.docdate Ved_date,
       --          rang.name rang,
       
       --    UDO_F_GET_FOT_ONDATE(Nprn      => fm.rn,
       --                         Sgrsalary => '������� ���� (��)',
       --                         Ddate     => greatest(TRUNC(t.docdate, 'Month'),
       --                                                cp.jobbegin_date)) visotokMON_Add,
       ts.month,
       fm.rn    fm_rn
      
        from slpsheets t,
             slpsheetspfm tp,
             slpsheetspfmspecs ts,
             slcompcharges sc,
             slcompgr sg,
             slcompgrstruct sgs,
             clnpspfm fm,
             clnpsdep cdep, --added
             clnposts p,
             clnpersons cp,
             agnlist al,
             agnranks agr, --added
             SLRANK rang, --added
             V_INS_DEPARTMENT D,
             jurpersons u,
             doctypes dt,
             SLCALCULAT scal,
             SLANLSIGNS sanl,
             Officercls ofc,
             (select * from selectlist sls where sls.ident = IIDENT) slsl
      
       where t.rn = tp.prn
         and t.rn = slsl.document -- NVed -- 2000563654 --sl.document
         and tp.rn = ts.prn
         and t.jurpersons = u.rn
         and tp.CLNPSPFM = fm.rn
         and fm.persrn = cp.rn
         and fm.officercls = ofc.rn(+)
         and fm.psdeprn = cdep.rn(+) --added      
         and cp.pers_agent = al.rn
         and al.rn = agr.prn(+) --added
         and agr.rank = rang.rn
         and fm.postrn = p.rn(+)
         and ts.slcompcharges = sc.rn
         and substr(sg.code, 1, 2) = sPAY_GROUP_ID 
            --   or  sg.code = '5 �������������+���')
         and t.doctypes = dt.rn
            --and (sDEPCODE is null or instr(sDEPCODE, D.code) > 0)
         and sg.rn = sgs.prn(+)
         and fm.DEPTRN = D.RN(+)
         and sgs.slcompcharges = ts.slcompcharges(+)
         and t.slcalculat = scal.rn
         and ts.slanlsigns4 = sanl.rn(+)
       order by al.agnname;
    commit;
  
    open CUR for
      select GGG.*,
             (select TRUNC(MAX(utff.docdate), 'Month')
                from UDO_T_VED_MO_FOR_FR2 utff) TargetDate, --,'Month') ,,
             UDO_PKG_SALARY_VED_FOR_MOV2.UDO_F_GET_FOT_ONDATE(Nprn      => GGG.Fm_Rn,
                                                              Sgrsalary => '������� ���� (��)',
                                                              Ddate     => greatest(TRUNC(GGG.DOCDATE,
                                                                                          'Month'),
                                                                                    GGG.jobbegin_date)) visotokMON_Add,
             UDO_PKG_SALARY_VED_FOR_MOV2.FU_AGNRANK_ONDATE(NAGNLIST => GGG.agn_rn,
                                                           DDATE    => TRUNC(GGG.docdate,
                                                                             'Month')) Rank,
             UDO_PKG_SALARY_VED_FOR_MOV2.FU_GET_PREMPFLS_EXP_ONDATE(NAGNLIST => GGG.agn_rn,
                                                                    dondate  => TRUNC(GGG.docdate,
                                                                                      'Month')) EXP_ONDATE
        from (select distinct utf.fullagnname,
                              utf.vrn,
                              utf.docdate,
                              utf.jobbegin_date,
                              utf.shtatna_posada,
                              utf.ofc_numb,
                              utf.ofc_name,
                              --   utf.summa,
                              --   utf.gr_paypaid_name,
                              --   utf.gr_paypaid_code,
                              utf.agn_rn,
                              utf.agnname,
                              utf.fm_rn
              
              --    utf.month
                from UDO_T_VED_MO_FOR_FR2 utf) GGG
       order by GGG.fullagnname;
  
  end GetPersonalData_A3______;

  ------------------------------------------------------------------------

  procedure Get_GRAddsVal_A4( /*NVed*/IIDENT  in number,
                             AGNRN   in number,
                             sSgCode in varchar2,
                             CUR     in out sys_refcursor) as
  begin
    open CUR for
      select sum(BBBB.SUMMA) SUMMA,
             BBBB.agnname,
             BBBB.agn_rn,
             substr(BBBB.gr_payPaid_name, 1, 2) OrderBy,
             substr(BBBB.gr_payPaid_name, 3) gr_payPaid_name,
             BBBB.gr_payPaid_code
        from (select distinct sc.Name Code,
                              sc.numb,
                              --         sc.compch_type markType_PayPaid,
                              ts.summ  summa,
                              ts.month,
                              ---      sum  (ts.summ) summa,
                              sg.name gr_payPaid_name,
                              sg.code gr_payPaid_code,
                              --         agr.begin_date rang_begin_date,
                              al.rn      agn_rn,
                              al.agnname --,
              
                from -- (select sellist.document from selectlist sellist where sellist.ident=IDENT) sl,
                     slpsheets t,
                     slpsheetspfm tp,
                     slpsheetspfmspecs ts,
                     slcompcharges sc,
                     slcompgr sg,
                     slcompgrstruct sgs,
                     clnpspfm fm,
                     clnpsdep cdep, --added
                     clnposts p,
                     clnpersons cp,
                     agnlist al,
                     agnranks agr, --added
                     SLRANK rang, --added
                     V_INS_DEPARTMENT D,
                     jurpersons u,
                     doctypes dt,
                     SLCALCULAT scal,
                     SLANLSIGNS sanl,
                     (select * from selectlist slsl where slsl.ident = IIDENT) slsl
               where t.rn = tp.prn
                 and t.rn = slsl.document --NVed --  2000563654 --sl.document
                 and tp.rn = ts.prn
                 and t.jurpersons = u.rn
                 and tp.CLNPSPFM = fm.rn
                 and fm.persrn = cp.rn
                 and fm.psdeprn = cdep.rn(+) --added
                 and cp.pers_agent = al.rn
                 and al.rn = agr.prn(+) --added
                 and agr.rank = rang.rn
                 and fm.postrn = p.rn(+)
                 and ts.slcompcharges = sc.rn
                 and (substr(sg.code, 1, 2) = sPAY_GROUP_ID ) -- or sg.code='5 �������������+���' or sc.numb=71 )
                 and t.doctypes = dt.rn
                    --and (sDEPCODE is null or instr(sDEPCODE, D.code) > 0)
                 and sg.rn = sgs.prn(+)
                 and fm.DEPTRN = D.RN(+)
                 and sgs.slcompcharges = ts.slcompcharges(+)
                 and t.slcalculat = scal.rn
                 and ts.slanlsigns4 = sanl.rn(+)
                 and sc.compch_type = 10
                 and sg.code = sSgCode
                 and al.rn = AGNRN
              
              ) BBBB
       group by BBBB.agnname,
                BBBB.agn_rn,
                substr(BBBB.gr_payPaid_name, 1, 2),
                substr(BBBB.gr_payPaid_name, 3),
                BBBB.gr_payPaid_code;
  
  end Get_GRAddsVal_A4;

  -----------------------------------------------------------------

  procedure Get_GROutsVal_A4(IIDENT /*NVed*/  in number,
                             AGNRN   in number,
                             sSgCode in varchar2,
                             CUR     in out sys_refcursor) as
  begin
    open CUR for
      select sum(BBBB.SUMMA) SUMMA,
             BBBB.agnname,
             BBBB.agn_rn,
             substr(BBBB.gr_payPaid_name, 1, 2) OrderBy,
             substr(BBBB.gr_payPaid_name, 3) gr_payPaid_name,
             BBBB.gr_payPaid_code
        from (select distinct sc.Name Code,
                              sc.numb,
                              --         sc.compch_type markType_PayPaid,
                              ts.summ  summa,
                              ts.month,
                              ---      sum  (ts.summ) summa,
                              sg.name gr_payPaid_name,
                              sg.code gr_payPaid_code,
                              --         agr.begin_date rang_begin_date,
                              al.rn      agn_rn,
                              al.agnname --,
              --           t.docdate Ved_date,
              --          rang.name rang,
              --         'StatVal 0%' visotokMON_Add 
              --  decode(substr(sg.code, 4, 2), '50', 99, sc.compch_type) compch_type,
              --     decode(substr(sg.code, 4, 2), '99', 99,
              --            sc.compch_type) compch_type,
              --     al.rn,             --al.agnname,
              --     tp.clnpspfm,
              --     t.docdate, --trunc(t.docdate, 'MM'),
              --     sum(ts.summ) nSum,
              --     tp.SUMMCHARDE,
              --     tp.SUMMRETAIN,
              --     nvl(tp.SUMMCHARDE, 0) + nvl(tp.SUMMADVANCE, 0) -
              --     nvl(tp.SUMMRETAIN, 0) + nvl(tp.SUMMOVERPAY, 0) nSUMAll,
              --     D.name,
              --     D.rn,
              --     t.rn,
              --     t.docdate,
              --     dt.doccode,
              --     scal.year VED_YEAR,
              --     scal.monthnumb VED_MONTH,
              --     t.docdate VED_DATE,
              --     TS.SLCOMPCHARGES NSLCOMPCH /*z*/
              --    into  nRESULT
                from -- (select sellist.document from selectlist sellist where sellist.ident=IDENT) sl,
                     slpsheets t,
                     slpsheetspfm tp,
                     slpsheetspfmspecs ts,
                     slcompcharges sc,
                     slcompgr sg,
                     slcompgrstruct sgs,
                     clnpspfm fm,
                     clnpsdep cdep, --added
                     clnposts p,
                     clnpersons cp,
                     agnlist al,
                     agnranks agr, --added
                     SLRANK rang, --added
                     V_INS_DEPARTMENT D,
                     jurpersons u,
                     doctypes dt,
                     SLCALCULAT scal,
                     SLANLSIGNS sanl,
                     (select * from selectlist slsl where slsl.ident = IIDENT) slsl
               where t.rn = tp.prn
                 and t.rn = slsl.document --NVed --  2000563654 --sl.document
                 and tp.rn = ts.prn
                 and t.jurpersons = u.rn
                 and tp.CLNPSPFM = fm.rn
                 and fm.persrn = cp.rn
                 and fm.psdeprn = cdep.rn(+) --added
                 and cp.pers_agent = al.rn
                 and al.rn = agr.prn(+) --added
                 and agr.rank = rang.rn
                 and fm.postrn = p.rn(+)
                 and ts.slcompcharges = sc.rn
                 and (substr(sg.code, 1, 2) = sPAY_GROUP_ID ) -- or sg.code='5 �������������+���' or sc.numb=71 )
                 and t.doctypes = dt.rn
                    --and (sDEPCODE is null or instr(sDEPCODE, D.code) > 0)
                 and sg.rn = sgs.prn(+)
                 and fm.DEPTRN = D.RN(+)
                 and sgs.slcompcharges = ts.slcompcharges(+)
                 and t.slcalculat = scal.rn
                 and ts.slanlsigns4 = sanl.rn(+)
                 and sc.compch_type = 30
                 and sg.code = sSgCode
                 and al.rn = AGNRN
              --   group by
              -- sg.code,
              -- sg.name ,
              -- sc.Name , sc.numb, al.agnname
              ) BBBB
       group by BBBB.agnname,
                BBBB.agn_rn,
                substr(BBBB.gr_payPaid_name, 1, 2),
                substr(BBBB.gr_payPaid_name, 3),
                BBBB.gr_payPaid_code;
  
  end Get_GROutsVal_A4;

  ---------------------------------------------------------------------------------

  function GetCOUNTHead_GR_Adds(IIDENT /*NVed*/ in number) return number as
    nRESULT number(10);
  begin
    begin
      select count(*)
        into nRESULT
        from (select sum(BBBB.SUMMA) SUMMA,
                     substr(BBBB.gr_payPaid_name, 1, 2) OrderBy,
                     substr(BBBB.gr_payPaid_name, 3) gr_payPaid_name,
                     BBBB.gr_payPaid_code
                from (select distinct sc.Name Code,
                                      sc.numb, --,        
                                      --         sc.compch_type markType_PayPaid,
                                      ts.summ summa,
                                      ---      sum  (ts.summ) summa,
                                      sg.name    gr_payPaid_name,
                                      sg.code    gr_payPaid_code,
                                      al.agnname --,
                      
                        from -- (select sellist.document from selectlist sellist where sellist.ident=IDENT) sl,
                             slpsheets t,
                             slpsheetspfm tp,
                             slpsheetspfmspecs ts,
                             slcompcharges sc,
                             slcompgr sg,
                             slcompgrstruct sgs,
                             clnpspfm fm,
                             clnpsdep cdep, --added
                             clnposts p,
                             clnpersons cp,
                             agnlist al,
                             agnranks agr, --added
                             SLRANK rang, --added
                             V_INS_DEPARTMENT D,
                             jurpersons u,
                             doctypes dt,
                             SLCALCULAT scal,
                             SLANLSIGNS sanl,
                             (select *
                                from selectlist slsl
                               where slsl.ident = IIDENT) slsl
                       where t.rn = tp.prn
                         and t.rn = slsl.document -- NVed --2000563654 --sl.document
                         and tp.rn = ts.prn
                         and t.jurpersons = u.rn
                         and tp.CLNPSPFM = fm.rn
                         and fm.persrn = cp.rn
                         and fm.psdeprn = cdep.rn(+) --added
                         and cp.pers_agent = al.rn
                         and al.rn = agr.prn(+) --added
                         and agr.rank = rang.rn
                         and fm.postrn = p.rn(+)
                         and ts.slcompcharges = sc.rn
                         and (substr(sg.code, 1, 2) = sPAY_GROUP_ID ) -- or sg.code='5 �������������+���' or sc.numb=71 )
                         and t.doctypes = dt.rn
                            --and (sDEPCODE is null or instr(sDEPCODE, D.code) > 0)
                         and sg.rn = sgs.prn(+)
                         and fm.DEPTRN = D.RN(+)
                         and sgs.slcompcharges = ts.slcompcharges(+)
                         and t.slcalculat = scal.rn
                         and ts.slanlsigns4 = sanl.rn(+)
                         and sc.compch_type = 10
                      --   group by
                      -- sg.code,
                      -- sg.name ,
                      -- sc.Name , sc.numb, al.agnname
                      ) BBBB
               group by BBBB.gr_payPaid_name, BBBB.gr_payPaid_code) LLLL
       where LLLL.SUMMA <> 0
       order by LLLL.OrderBy;
    end;
    return(nRESULT);
  
  end GetCOUNTHead_GR_Adds;

  -----------------------------------------------------------------------

  function GetCOUNTHead_GR_Outs(IIDENT /*NVed*/ in number) return number as
    nRESULT number(10);
  begin
    begin
      select count(*)
        into nRESULT
        from (select sum(BBBB.SUMMA) SUMMA,
                     substr(BBBB.gr_payPaid_name, 1, 2) OrderBy,
                     substr(BBBB.gr_payPaid_name, 3) gr_payPaid_name,
                     BBBB.gr_payPaid_code
                from (select distinct sc.Name Code,
                                      sc.numb, --,        
                                      --         sc.compch_type markType_PayPaid,
                                      ts.summ summa,
                                      ---      sum  (ts.summ) summa,
                                      sg.name    gr_payPaid_name,
                                      sg.code    gr_payPaid_code,
                                      al.agnname --,
                      
                        from -- (select sellist.document from selectlist sellist where sellist.ident=IDENT) sl,
                             slpsheets t,
                             slpsheetspfm tp,
                             slpsheetspfmspecs ts,
                             slcompcharges sc,
                             slcompgr sg,
                             slcompgrstruct sgs,
                             clnpspfm fm,
                             clnpsdep cdep, --added
                             clnposts p,
                             clnpersons cp,
                             agnlist al,
                             agnranks agr, --added
                             SLRANK rang, --added
                             V_INS_DEPARTMENT D,
                             jurpersons u,
                             doctypes dt,
                             SLCALCULAT scal,
                             SLANLSIGNS sanl,
                             (select *
                                from selectlist slsl
                               where slsl.ident = IIDENT) slsl
                       where t.rn = tp.prn
                         and t.rn = slsl.document --NVed --2000563654 --sl.document
                         and tp.rn = ts.prn
                         and t.jurpersons = u.rn
                         and tp.CLNPSPFM = fm.rn
                         and fm.persrn = cp.rn
                         and fm.psdeprn = cdep.rn(+) --added
                         and cp.pers_agent = al.rn
                         and al.rn = agr.prn(+) --added
                         and agr.rank = rang.rn
                         and fm.postrn = p.rn(+)
                         and ts.slcompcharges = sc.rn
                         and (substr(sg.code, 1, 2) = sPAY_GROUP_ID ) -- or sg.code='5 �������������+���' or sc.numb=71 )
                         and (substr(sg.code, 4, 2) not in ('88', '99'))
                         and t.doctypes = dt.rn
                            --and (sDEPCODE is null or instr(sDEPCODE, D.code) > 0)
                         and sg.rn = sgs.prn(+)
                         and fm.DEPTRN = D.RN(+)
                         and sgs.slcompcharges = ts.slcompcharges(+)
                         and t.slcalculat = scal.rn
                         and ts.slanlsigns4 = sanl.rn(+)
                         and sc.compch_type = 30
                      --   group by
                      -- sg.code,
                      -- sg.name ,
                      -- sc.Name , sc.numb, al.agnname
                      ) BBBB
               group by BBBB.gr_payPaid_name, BBBB.gr_payPaid_code) LLLL
       where LLLL.SUMMA <> 0
       order by LLLL.OrderBy;
    end;
    return(nRESULT);
  
  end GetCOUNTHead_GR_Outs;

  procedure GetHeadsAddsGr_A6( /*NVed*/IIDENT in number,
                              CUR    in out sys_refcursor) as
  begin
    open CUR for
      select LLLL.*
        from (select sum(BBBB.SUMMA) SUMMA,
                     substr(BBBB.gr_payPaid_name, 1, 2) OrderBy,
                     substr(BBBB.gr_payPaid_name, 3) gr_payPaid_name,
                     BBBB.gr_payPaid_code
                from (select distinct sc.Name Code,
                                      sc.numb, --,        
                                      --         sc.compch_type markType_PayPaid,
                                      ts.summ summa,
                                      ---      sum  (ts.summ) summa,
                                      sg.name gr_payPaid_name,
                                      sg.code gr_payPaid_code,
                                      --         agr.begin_date rang_begin_date,
                                      --         al.rn agn_rn,
                                      al.agnname --,
                      
                        from -- (select sellist.document from selectlist sellist where sellist.ident=IDENT) sl,
                             slpsheets t,
                             slpsheetspfm tp,
                             slpsheetspfmspecs ts,
                             slcompcharges sc,
                             slcompgr sg,
                             slcompgrstruct sgs,
                             clnpspfm fm,
                             clnpsdep cdep, --added
                             clnposts p,
                             clnpersons cp,
                             agnlist al,
                             agnranks agr, --added
                             SLRANK rang, --added
                             V_INS_DEPARTMENT D,
                             jurpersons u,
                             doctypes dt,
                             SLCALCULAT scal,
                             SLANLSIGNS sanl,
                             (select *
                                from selectlist slsl
                               where slsl.ident = IIDENT) slsl
                       where t.rn = tp.prn
                         and t.rn = slsl.document --NVed --sl.document
                         and tp.rn = ts.prn
                         and t.jurpersons = u.rn
                         and tp.CLNPSPFM = fm.rn
                         and fm.persrn = cp.rn
                         and fm.psdeprn = cdep.rn(+) --added
                         and cp.pers_agent = al.rn
                         and al.rn = agr.prn(+) --added
                         and agr.rank = rang.rn
                         and fm.postrn = p.rn(+)
                         and ts.slcompcharges = sc.rn
                         and (substr(sg.code, 1, 2) = sPAY_GROUP_ID ) -- or sg.code='5 �������������+���' or sc.numb=71 )
                         and t.doctypes = dt.rn
                            --and (sDEPCODE is null or instr(sDEPCODE, D.code) > 0)
                         and sg.rn = sgs.prn(+)
                         and fm.DEPTRN = D.RN(+)
                         and sgs.slcompcharges = ts.slcompcharges(+)
                         and t.slcalculat = scal.rn
                         and ts.slanlsigns4 = sanl.rn(+)
                         and sc.compch_type = 10) BBBB
               group by BBBB.gr_payPaid_name, BBBB.gr_payPaid_code) LLLL
       where LLLL.SUMMA <> 0
       order by LLLL.OrderBy;
  
  end GetHeadsAddsGr_A6;

  ------------------------------------------------------

  procedure GetHeadsOutsGr_A6( /*NVed*/IIDENT in number,
                              CUR    in out sys_refcursor) as
  begin
    open CUR for
      select LLLL.*
        from (select sum(BBBB.SUMMA) SUMMA,
                     substr(BBBB.gr_payPaid_name, 1, 2) OrderBy,
                     substr(BBBB.gr_payPaid_name, 3) gr_payPaid_name,
                     BBBB.gr_payPaid_code
                from (select distinct sc.Name Code,
                                      sc.numb, --,        
                                      --         sc.compch_type markType_PayPaid,
                                      ts.summ  summa,
                                      ts.month,
                                      ---      sum  (ts.summ) summa,
                                      sg.name gr_payPaid_name,
                                      sg.code gr_payPaid_code,
                                      --         agr.begin_date rang_begin_date,
                                      --         al.rn agn_rn,
                                      al.agnname --,
                      
                        from -- (select sellist.document from selectlist sellist where sellist.ident=IDENT) sl,
                             slpsheets t,
                             slpsheetspfm tp,
                             slpsheetspfmspecs ts,
                             slcompcharges sc,
                             slcompgr sg,
                             slcompgrstruct sgs,
                             clnpspfm fm,
                             clnpsdep cdep, --added
                             clnposts p,
                             clnpersons cp,
                             agnlist al,
                             agnranks agr, --added
                             SLRANK rang, --added
                             V_INS_DEPARTMENT D,
                             jurpersons u,
                             doctypes dt,
                             SLCALCULAT scal,
                             SLANLSIGNS sanl,
                             (select *
                                from selectlist slsl
                               where slsl.ident = IIDENT) slsl
                       where t.rn = tp.prn
                         and t.rn = slsl.document --NVed --sl.document
                         and tp.rn = ts.prn
                         and t.jurpersons = u.rn
                         and tp.CLNPSPFM = fm.rn
                         and fm.persrn = cp.rn
                         and fm.psdeprn = cdep.rn(+) --added
                         and cp.pers_agent = al.rn
                         and al.rn = agr.prn(+) --added
                         and agr.rank = rang.rn
                         and fm.postrn = p.rn(+)
                         and ts.slcompcharges = sc.rn
                         and (substr(sg.code, 1, 2) = sPAY_GROUP_ID ) -- or sg.code='5 �������������+���' or sc.numb=71 )
                         and (substr(sg.code, 4, 2) not in ('88', '99')) --and (substr(sg.code, 4, 2) <> '88')
                         and t.doctypes = dt.rn
                            --and (sDEPCODE is null or instr(sDEPCODE, D.code) > 0)
                         and sg.rn = sgs.prn(+)
                         and fm.DEPTRN = D.RN(+)
                         and sgs.slcompcharges = ts.slcompcharges(+)
                         and t.slcalculat = scal.rn
                         and ts.slanlsigns4 = sanl.rn(+)
                         and sc.compch_type = 30) BBBB
               group by BBBB.gr_payPaid_name, BBBB.gr_payPaid_code) LLLL
       where LLLL.SUMMA <> 0
       order by LLLL.OrderBy;
  
  end GetHeadsOutsGr_A6;

  -------------------------------------------------------------------

  Procedure Get_category_cursor_A2(aCursor out pkg_cursors.CurType) as
  begin
    open aCursor for
      select distinct bbb.Ofc_Name,
                      bbb.ofc_numb,
                      sum(bbb.suum) over(partition by bbb.Ofc_Name) Pers_count
        from (select S_AT.Ofc_Name,
                     S_AT.ofc_numb,
                     count(*) Over(partition by S_AT.agn_rn) suum
                from (select distinct utf.agn_rn, utf.ofc_name, utf.ofc_numb
                        from UDO_T_VED_MO_FOR_FR2 utf,
                             (select distinct t.ofc_name, t.ofc_numb
                                from UDO_T_VED_MO_FOR_FR2 t) utf2
                       where utf.ofc_numb = utf2.ofc_numb) S_AT) bbb
       order by bbb.ofc_numb;
  
  end;

  -------------------------------------------------------------------
  function Get_Val_By_GR(ofc_numbb in number, sSgCode in varchar2)
    return number as
    nResult number(17, 2);
  begin
    begin
      select sum(t.summa)
        into nResult
        from (select distinct * from UDO_T_VED_MO_FOR_FR2) t
       where t.ofc_numb = ofc_numbb
         and t.gr_paypaid_code = sSgCode;
    
    end;
    return(nResult);
  end Get_Val_By_GR;

  ---------------------------------------------------------------------

  function GetSummAddsByAgnRn( /*NVed*/ IIDENT in number, AGNRN in number)
    return number as
    nResult number(17, 2);
  begin
    begin
      select sum(ttt.summ)
        into nResult
        from (select distinct al.rn agn_rn,
                              ts.summ,
                              ts.month,
                              sc.name,
                              al.agnname
                from -- (select sellist.document from selectlist sellist where sellist.ident=IDENT) sl,
                     slpsheets t,
                     slpsheetspfm tp,
                     slpsheetspfmspecs ts,
                     slcompcharges sc,
                     slcompgr sg,
                     slcompgrstruct sgs,
                     clnpspfm fm,
                     clnpsdep cdep, --added
                     clnposts p,
                     clnpersons cp,
                     agnlist al,
                     agnranks agr, --added
                     SLRANK rang, --added
                     V_INS_DEPARTMENT D,
                     jurpersons u,
                     doctypes dt,
                     SLCALCULAT scal,
                     SLANLSIGNS sanl,
                     (select * from selectlist slsl where slsl.ident = IIDENT) slsl
               where t.rn = tp.prn
                 and t.rn = slsl.document --NVed --sl.document
                 and tp.rn = ts.prn
                 and t.jurpersons = u.rn
                 and tp.CLNPSPFM = fm.rn
                 and fm.persrn = cp.rn
                 and fm.psdeprn = cdep.rn(+) --added
                 and cp.pers_agent = al.rn
                 and al.rn = agr.prn(+) --added
                 and agr.rank = rang.rn
                 and fm.postrn = p.rn(+)
                 and ts.slcompcharges = sc.rn
                    --     and (substr(sg.code, 1, 2) = '49' or sg.code='5 �������������+���' or sc.numb=71)
                 and t.doctypes = dt.rn
                    --and (sDEPCODE is null or instr(sDEPCODE, D.code) > 0)
                 and sg.rn = sgs.prn(+)
                 and fm.DEPTRN = D.RN(+)
                 and sgs.slcompcharges = ts.slcompcharges(+)
                 and t.slcalculat = scal.rn
                 and ts.slanlsigns4 = sanl.rn(+)
                 and sc.compch_type = 10
                 and al.rn = AGNRN --858748114
              --   and sc.numb = nNumb
              --  group by   al.rn, al.agnname ,ts.summ,
              --  sc.name
              /*numm(t.pref),
                   trim(t.numb),
                  sg.name,
                  cdep.psdep_name,
                  sg.code,
                  sc.compch_type,
                  al.rn,
               --   al.agnname,
                  tp.clnpspfm,
                  t.docdate, --trunc(t.docdate, 'MM'),
                  tp.SUMMCHARDE,
                  tp.SUMMRETAIN,
                  nvl(tp.SUMMCHARDE, 0) + nvl(tp.SUMMADVANCE, 0) -
                  nvl(tp.SUMMRETAIN, 0) + nvl(tp.SUMMOVERPAY, 0),
                  D.name,
                  D.rn,
                  t.rn,
                  t.docdate,
                  dt.doccode,
                scal.year,
              scal.monthnumb,
              t.docdate,
                  TS.SLCOMPCHARGES  a*/
               order by al.rn) ttt;
    end;
    return(nResult);
  end GetSummAddsByAgnRn;

  ------------------------------------------------------------------------

  function GetSummAddsBy_Category( /*NVed*/IIDENT    in number,
                                  Ofc_numbb in number) return number as
    nResult number(17, 2);
  begin
    begin
      select sum(ttt.summ)
        into nResult
        from (select distinct al.rn agn_rn,
                              ts.summ,
                              ts.month,
                              sc.name,
                              al.agnname
                from -- (select sellist.document from selectlist sellist where sellist.ident=IDENT) sl,
                     slpsheets t,
                     slpsheetspfm tp,
                     slpsheetspfmspecs ts,
                     slcompcharges sc,
                     slcompgr sg,
                     slcompgrstruct sgs,
                     clnpspfm fm,
                     clnpsdep cdep, --added
                     clnposts p,
                     clnpersons cp,
                     agnlist al,
                     agnranks agr, --added
                     SLRANK rang, --added
                     V_INS_DEPARTMENT D,
                     jurpersons u,
                     doctypes dt,
                     SLCALCULAT scal,
                     OFFICERCLS ofc,
                     SLANLSIGNS sanl,
                     (select * from selectlist slsl where slsl.ident = IIDENT) slsl
               where t.rn = tp.prn
                 and t.rn = slsl.document --NVed --sl.document
                 and tp.rn = ts.prn
                 and t.jurpersons = u.rn
                 and tp.CLNPSPFM = fm.rn
                 and fm.persrn = cp.rn
                 and fm.psdeprn = cdep.rn(+) --added
                 and cp.pers_agent = al.rn
                 and al.rn = agr.prn(+) --added
                 and agr.rank = rang.rn
                 and fm.postrn = p.rn(+)
                 and ts.slcompcharges = sc.rn
                    --     and (substr(sg.code, 1, 2) = '49' or sg.code='5 �������������+���' or sc.numb=71)
                 and t.doctypes = dt.rn
                    --and (sDEPCODE is null or instr(sDEPCODE, D.code) > 0)
                 and sg.rn = sgs.prn(+)
                 and fm.DEPTRN = D.RN(+)
                 and sgs.slcompcharges = ts.slcompcharges(+)
                 and t.slcalculat = scal.rn
                 and ts.slanlsigns4 = sanl.rn(+)
                 and sc.compch_type = 10
                 and fm.officercls = ofc.rn(+)
                 and ofc.ordnumb = Ofc_numbb
              
               order by al.rn) ttt;
    end;
    return(nResult);
  end GetSummAddsBy_Category;
  -------------------------------------------------------------------

  function GetSummOutSBy_Category( /*NVed*/IIDENT    in number,
                                  Ofc_numbb in number) return number as
    nResult number(17, 2);
  begin
    begin
      select sum(ttt.summ)
        into nResult
        from (select distinct al.rn agn_rn,
                              ts.summ,
                              ts.month,
                              sc.name,
                              al.agnname
                from -- (select sellist.document from selectlist sellist where sellist.ident=IDENT) sl,
                     slpsheets t,
                     slpsheetspfm tp,
                     slpsheetspfmspecs ts,
                     slcompcharges sc,
                     slcompgr sg,
                     slcompgrstruct sgs,
                     clnpspfm fm,
                     clnpsdep cdep, --added
                     clnposts p,
                     clnpersons cp,
                     agnlist al,
                     agnranks agr, --added
                     SLRANK rang, --added
                     V_INS_DEPARTMENT D,
                     jurpersons u,
                     doctypes dt,
                     SLCALCULAT scal,
                     OFFICERCLS ofc,
                     SLANLSIGNS sanl,
                     (select * from selectlist slsl where slsl.ident = IIDENT) slsl
               where t.rn = tp.prn
                 and t.rn = slsl.document --NVed --sl.document
                 and tp.rn = ts.prn
                 and t.jurpersons = u.rn
                 and tp.CLNPSPFM = fm.rn
                 and fm.persrn = cp.rn
                 and fm.psdeprn = cdep.rn(+) --added
                 and cp.pers_agent = al.rn
                 and al.rn = agr.prn(+) --added
                 and agr.rank = rang.rn
                 and fm.postrn = p.rn(+)
                 and ts.slcompcharges = sc.rn
                 and (substr(sg.code, 1, 2) = sPAY_GROUP_ID ) -- or sg.code='5 �������������+���' or sc.numb=71)
                 and (substr(sg.code, 4, 2) not in ('88', '99'))
                 and t.doctypes = dt.rn
                    --and (sDEPCODE is null or instr(sDEPCODE, D.code) > 0)
                 and sg.rn = sgs.prn(+)
                 and fm.DEPTRN = D.RN(+)
                 and sgs.slcompcharges = ts.slcompcharges(+)
                 and t.slcalculat = scal.rn
                 and ts.slanlsigns4 = sanl.rn(+)
                 and sc.compch_type = 30
                 and fm.officercls = ofc.rn(+)
                 and ofc.ordnumb = Ofc_numbb
              
               order by al.rn) ttt;
    end;
    return(nResult);
  end GetSummOutSBy_Category;

  ---------------------------------------------------------------------------

  function GetSummK_PDFO_By_Category( /*NVed*/IIDENT    in number,
                                     Ofc_numbb in number) return number as
    nResult number(17, 2);
  begin
    begin
      select sum(ttt.summ)
        into nResult
        from (select distinct al.rn agn_rn,
                              ts.summ,
                              ts.month,
                              sc.name,
                              al.agnname
                from -- (select sellist.document from selectlist sellist where sellist.ident=IDENT) sl,
                     slpsheets t,
                     slpsheetspfm tp,
                     slpsheetspfmspecs ts,
                     slcompcharges sc,
                     slcompgr sg,
                     slcompgrstruct sgs,
                     clnpspfm fm,
                     clnpsdep cdep, --added
                     clnposts p,
                     clnpersons cp,
                     agnlist al,
                     agnranks agr, --added
                     SLRANK rang, --added
                     V_INS_DEPARTMENT D,
                     jurpersons u,
                     doctypes dt,
                     SLCALCULAT scal,
                     OFFICERCLS ofc,
                     SLANLSIGNS sanl,
                     (select * from selectlist slsl where slsl.ident = IIDENT) slsl
               where t.rn = tp.prn
                 and t.rn = slsl.document --NVed --sl.document
                 and tp.rn = ts.prn
                 and t.jurpersons = u.rn
                 and tp.CLNPSPFM = fm.rn
                 and fm.persrn = cp.rn
                 and fm.psdeprn = cdep.rn(+) --added
                 and cp.pers_agent = al.rn
                 and al.rn = agr.prn(+) --added
                 and agr.rank = rang.rn
                 and fm.postrn = p.rn(+)
                 and ts.slcompcharges = sc.rn
                 and (substr(sg.code, 1, 2) = sPAY_GROUP_ID ) -- or sg.code='5 �������������+���' or sc.numb=71)
                 and (substr(sg.code, 4, 2) = '88')
                 and t.doctypes = dt.rn
                    --and (sDEPCODE is null or instr(sDEPCODE, D.code) > 0)
                 and sg.rn = sgs.prn(+)
                 and fm.DEPTRN = D.RN(+)
                 and sgs.slcompcharges = ts.slcompcharges(+)
                 and t.slcalculat = scal.rn
                 and ts.slanlsigns4 = sanl.rn(+)
                 and sc.compch_type = 30
                 and fm.officercls = ofc.rn(+)
                 and ofc.ordnumb = Ofc_numbb
              --  and al.rn = AGNRN --858748114
              --   and sc.numb = nNumb
              --  group by   al.rn, al.agnname ,ts.summ,
              --  sc.name
              /*numm(t.pref),
                   trim(t.numb),
                  sg.name,
                  cdep.psdep_name,
                  sg.code,
                  sc.compch_type,
                  al.rn,
               --   al.agnname,
                  tp.clnpspfm,
                  t.docdate, --trunc(t.docdate, 'MM'),
                  tp.SUMMCHARDE,
                  tp.SUMMRETAIN,
                  nvl(tp.SUMMCHARDE, 0) + nvl(tp.SUMMADVANCE, 0) -
                  nvl(tp.SUMMRETAIN, 0) + nvl(tp.SUMMOVERPAY, 0),
                  D.name,
                  D.rn,
                  t.rn,
                  t.docdate,
                  dt.doccode,
                scal.year,
              scal.monthnumb,
              t.docdate,
                  TS.SLCOMPCHARGES  a*/
               order by al.rn) ttt;
    end;
    return(nResult);
  end GetSummK_PDFO_By_Category;

  -----------------------------------------------------------
  function GetSumm_Bank_By_Category( /*NVed*/IIDENT    in number,
                                    Ofc_numbb in number) return number as
    nResult number(17, 2);
  begin
    begin
      select sum(ttt.summ)
        into nResult
        from (select distinct al.rn agn_rn,
                              ts.summ,
                              ts.month,
                              sc.name,
                              al.agnname
                from -- (select sellist.document from selectlist sellist where sellist.ident=IDENT) sl,
                     slpsheets t,
                     slpsheetspfm tp,
                     slpsheetspfmspecs ts,
                     slcompcharges sc,
                     slcompgr sg,
                     slcompgrstruct sgs,
                     clnpspfm fm,
                     clnpsdep cdep, --added
                     clnposts p,
                     clnpersons cp,
                     agnlist al,
                     agnranks agr, --added
                     SLRANK rang, --added
                     V_INS_DEPARTMENT D,
                     jurpersons u,
                     doctypes dt,
                     SLCALCULAT scal,
                     OFFICERCLS ofc,
                     SLANLSIGNS sanl,
                     (select * from selectlist slsl where slsl.ident = IIDENT) slsl
               where t.rn = tp.prn
                 and t.rn = slsl.document --NVed --sl.document
                 and tp.rn = ts.prn
                 and t.jurpersons = u.rn
                 and tp.CLNPSPFM = fm.rn
                 and fm.persrn = cp.rn
                 and fm.psdeprn = cdep.rn(+) --added
                 and cp.pers_agent = al.rn
                 and al.rn = agr.prn(+) --added
                 and agr.rank = rang.rn
                 and fm.postrn = p.rn(+)
                 and ts.slcompcharges = sc.rn
                 and (substr(sg.code, 1, 2) = sPAY_GROUP_ID ) -- or sg.code='5 �������������+���' or sc.numb=71)
                 and (substr(sg.code, 4, 2) = '99')
                 and t.doctypes = dt.rn
                    --and (sDEPCODE is null or instr(sDEPCODE, D.code) > 0)
                 and sg.rn = sgs.prn(+)
                 and fm.DEPTRN = D.RN(+)
                 and sgs.slcompcharges = ts.slcompcharges(+)
                 and t.slcalculat = scal.rn
                 and ts.slanlsigns4 = sanl.rn(+)
                 and sc.compch_type = 30
                 and fm.officercls = ofc.rn(+)
                 and ofc.ordnumb = Ofc_numbb
              
               order by al.rn) ttt;
    end;
    return(nResult);
  end GetSumm_Bank_By_Category;

  function GetSumm_Outs_By_AgnRn( /*NVed*/IIDENT in number,
                                 AGNRN  in number) return number as
    nResult number(17, 2);
  begin
    begin
      select sum(ttt.summ)
        into nResult
        from (select distinct al.rn agn_rn,
                              ts.summ,
                              ts.month,
                              sc.name,
                              al.agnname
                from -- (select sellist.document from selectlist sellist where sellist.ident=IDENT) sl,
                     slpsheets t,
                     slpsheetspfm tp,
                     slpsheetspfmspecs ts,
                     slcompcharges sc,
                     slcompgr sg,
                     slcompgrstruct sgs,
                     clnpspfm fm,
                     clnpsdep cdep, --added
                     clnposts p,
                     clnpersons cp,
                     agnlist al,
                     agnranks agr, --added
                     SLRANK rang, --added
                     V_INS_DEPARTMENT D,
                     jurpersons u,
                     doctypes dt,
                     SLCALCULAT scal,
                     SLANLSIGNS sanl,
                     (select * from selectlist slsl where slsl.ident = IIDENT) slsl
               where t.rn = tp.prn
                 and t.rn = slsl.document --NVed --sl.document
                 and tp.rn = ts.prn
                 and t.jurpersons = u.rn
                 and tp.CLNPSPFM = fm.rn
                 and fm.persrn = cp.rn
                 and fm.psdeprn = cdep.rn(+) --added
                 and cp.pers_agent = al.rn
                 and al.rn = agr.prn(+) --added
                 and agr.rank = rang.rn
                 and fm.postrn = p.rn(+)
                 and ts.slcompcharges = sc.rn
                 and (substr(sg.code, 1, 2) = sPAY_GROUP_ID ) -- or sg.code='5 �������������+���' or sc.numb=71)
                 and (substr(sg.code, 4, 2) not in ('88', '99'))
                 and t.doctypes = dt.rn
                    --and (sDEPCODE is null or instr(sDEPCODE, D.code) > 0)
                 and sg.rn = sgs.prn(+)
                 and fm.DEPTRN = D.RN(+)
                 and sgs.slcompcharges = ts.slcompcharges(+)
                 and t.slcalculat = scal.rn
                 and ts.slanlsigns4 = sanl.rn(+)
                 and sc.compch_type = 30
                 and al.rn = AGNRN --858748114
              --   and sc.numb = nNumb
              --  group by   al.rn, al.agnname ,ts.summ,
              --  sc.name
              /*numm(t.pref),
                   trim(t.numb),
                  sg.name,
                  cdep.psdep_name,
                  sg.code,
                  sc.compch_type,
                  al.rn,
               --   al.agnname,
                  tp.clnpspfm,
                  t.docdate, --trunc(t.docdate, 'MM'),
                  tp.SUMMCHARDE,
                  tp.SUMMRETAIN,
                  nvl(tp.SUMMCHARDE, 0) + nvl(tp.SUMMADVANCE, 0) -
                  nvl(tp.SUMMRETAIN, 0) + nvl(tp.SUMMOVERPAY, 0),
                  D.name,
                  D.rn,
                  t.rn,
                  t.docdate,
                  dt.doccode,
                scal.year,
              scal.monthnumb,
              t.docdate,
                  TS.SLCOMPCHARGES  a*/
               order by al.rn) ttt;
    end;
    return(nResult);
  end GetSumm_Outs_By_AgnRn;

  function GetSumm_ONHand_Category( /*NVed*/IIDENT    in number,
                                   Ofc_numbb in number) return number as
    nResult number(17, 2);
  begin
    begin
      nResult := GetSummAddsBy_Category(IIDENT    => IIDENT,
                                        Ofc_numbb => Ofc_numbb) -
                 GetSummOutSBy_Category(IIDENT    => IIDENT,
                                        Ofc_numbb => Ofc_numbb) -
                 GetSummK_PDFO_By_Category(IIDENT    => IIDENT,
                                           Ofc_numbb => Ofc_numbb) -
                 GetSumm_Bank_By_Category(IIDENT    => IIDENT,
                                          Ofc_numbb => Ofc_numbb);
    end;
    return(nResult);
  end GetSumm_ONHand_Category;

/* ��������  ������������
49�����               01�����
49����                01����
49���                 02���
49������              02����� �� ������
49��                  03��
49�������             03�������
49�������            04�������
49��������           04��������
49_88����             05����������� ����
49����������          05����������
49_99����             06����
49�����              06�����
49��������            07�������� ������
49������              08���������� �����
49ͳ���               09ͳ���
49���������� ������  10���������� ��������
49³�������           11³�������
49����������          12���������� ���

  */

/*
  -- Create table
create table UDO_T_VED_MO_FOR_FR2
(
  fullagnname     VARCHAR2(160),
  vrn             NUMBER(17) not null,
  docdate         DATE,
  jobbegin_date   DATE,
  shtatna_posada  VARCHAR2(240),
  ofc_numb        VARCHAR2(20),
  ofc_name        VARCHAR2(240),
  summa           NUMBER(17,2),
  gr_paypaid_name VARCHAR2(240),
  gr_paypaid_code VARCHAR2(240),
  agn_rn          NUMBER(17),
  agnname         VARCHAR2(240),
  month           NUMBER(10),
  fm_rn           NUMBER(17)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Grant/Revoke object privileges 
grant select on UDO_T_VED_MO_FOR_FR2 to PUBLIC;
  */

--begin
-- Initialization
-- <Statement>;
end UDO_PKG_SALARY_VED_FOR_MOV2;
/
